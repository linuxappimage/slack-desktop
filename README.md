# Slack Desktop

Slack Desktop Client for Linux

## AppImage

Download **Slack Desktop AppImage** binary from here: [Slack-x86_64.AppImage](https://gitlab.com/linuxappimage/slack-desktop/-/jobs/artifacts/master/raw/Slack-x86_64.AppImage?job=run-build)

### from you command line:

```bash
curl -sLo Slack-x86_64.AppImage https://gitlab.com/linuxappimage/slack-desktop/-/jobs/artifacts/master/raw/Slack-x86_64.AppImage?job=run-build

chmod +x Slack-x86_64.AppImage
./Slack-x86_64.AppImage

```
